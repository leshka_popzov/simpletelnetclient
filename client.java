import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.util.logging.Level;

class Reader extends Thread{
	private final SocketChannel sc;
	boolean quit = false;

	Reader(SocketChannel channel) {
		sc = channel;  
	}

	public void run() {
		boolean quit = false;
		while(!quit){
			Integer c = null;
			String cmd = null;
			
			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
			try {
				cmd = reader.readLine();
				sc.write(ByteBuffer.wrap(cmd.getBytes()));
				
				//	m_socket.getOutputStream().write(cmd.getBytes());

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}


		//		System.out.println(m_socket.getLocalPort());
		//	System.out.println(m_socket.getLocalAddress());
	}
}
//---------------------------------------------------------------


public class client {
	private static Socket m_socket;
	private static SocketChannel channel;

	public static boolean open(String addr,int port){
		try {
			m_socket = new Socket(addr,port);
		} catch (UnknownHostException e) {
			System.out.println("Unknown host");
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			System.out.println("connection error!");
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public static void close(){
		try {
			m_socket.close();
		} catch (IOException e) {

			e.printStackTrace();
		}
	}


	public static void main(String[] args) throws IOException {
		if(args.length>1 && open(args[0],Integer.valueOf(args[1]))){
			System.out.println("OK");
			close();
		}
		else{
			channel = SocketChannel.open();
			channel.connect(new InetSocketAddress("localhost",23));
			//	if(	open("localhost",23)){
			work();
			//}
		}
	}

	public static void ready(){
		System.out.print(">");
	}

	public static void work(){
		Reader IO = new Reader(channel);
		IO.start();
		OutputStreamWriter out = new OutputStreamWriter(System.out);
		try {
			ByteBuffer bb = ByteBuffer.allocate(1);
			while(true){
				while(channel.read(bb)>0){
					System.out.print((char)(((ByteBuffer) bb.flip()).get(0)));
					//System.out.print(' ');
					//bb.clear();
					
				}
			}
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
		/*
		
		
	}*/

}
